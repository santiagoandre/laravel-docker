# Laravel in docker
This project contains a docker configuration for a laravel project
#Features:
  - Apache as server
  - Mysql as database
  - Php 7.2
# Use:
  1. Clone this repo:
     `$ git clone https://gitlab.com/santiagandre/laravel-docker`

  2. Create a laravel project:
     `$ composer create-project --prefer-dist laravel/laravel example-project`

  3. Copy files of laravel-docker project(Dockerfile,docker-compose,init) in your project
	 `$ cp laravel-docker/* example-project/`

  4. Locate in your project
  5. Execute:
     `$ ./init name-app-container db-name-container`

     (you need add permissions to execute)
     
     (here, an .env-example file is created where the configuration of the name of the containers and the configured connection variables will be.)
  3. Configure env variables in .env(this cointains a example envs)
  4. Execute docker:
     `$ docker-compose up -d`

  5. Open  browser in localhost:8000

  ### Note: Any change in your project are reflected in the docker container.



## Helper scripts
Running `composer`, `php artisan` or `phpunit` against the `php` container with helper scripts in the main directory:

### container
Running `./container` takes you inside the `laravel-app` container under user uid(1000) (same with host user)

```
$ ./container
devuser@8cf37a093502:/var/www/html$
```
### db
Running `./db` connects to your database container's daemon using mysql client.
```
$ ./db
mysql>
```

### composer
Run `composer` command, example:
```
$ ./composer dump-autoload
Generating optimized autoload files> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: beyondcode/laravel-dump-server
Discovered Package: fideloper/proxy
Discovered Package: laravel/tinker
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Package manifest generated successfully.
Generated optimized autoload files containing 3527 classes
```

### php-artisan
Run `php artisan` commands, example:
```
$ ./php-artisan make:controller BlogPostController --resource
php artisan make:controller BlogPostController --resource
Controller created successfully.
```

### phpunit
Run `./vendor/bin/phpunit` to execute tests, example:

```
$ ./phpunit --group=failing
vendor/bin/phpunit --group=failing
PHPUnit 7.5.8 by Sebastian Bergmann and contributors.



Time: 34 ms, Memory: 6.00 MB

No tests executed!
```
